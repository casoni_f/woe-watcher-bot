class Player:
    def __init__(self, guid, account_id, name, online, staff, race, p_class, faction, ilvl=None):
        self.guid = guid
        self.account_id = account_id
        self.name = name
        self.online = online
        self.staff = staff
        self.race = race
        self.p_class = p_class
        self.faction = faction
        self.ilvl = ilvl

    def __init__(self, data: dict):
       self.guid = data.get('guid')
       self.account_id = data.get('account_id')
       self.name = data.get('name')
       self.online = data.get('online', False)
       self.staff = data.get('staff', False)
       self.race = data.get('race')
       self.p_class = data.get('class')
       self.faction = data.get('faction')
       self.ilvl = data.get('item_level', None)

    def _to_message(self):
        msg = "```json"
        msg += "\nPLAYER:\n"
        msg += "\tGuid: {}\n"
        msg += "\tName: {}\n"
        msg += "\tFaction: {}\n"
        msg += "\tRace: {}\n"
        msg += "\tClass: {}\n"
        msg += "\tIlvl: {}\n"
        msg += "\tStaff: {}\n"
        msg += "\tOnline: {}\n"
        msg += "```"
        return msg.format(self.guid, self.name, self.faction, self.race, self.p_class, self.ilvl, self.staff, self.online)
