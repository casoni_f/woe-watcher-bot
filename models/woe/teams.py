from models.woe.players import Player


class ArenaTeam:
    def __init__(self, id, name, rating, rank, captain):
        self.id = id
        self.name = name
        self.rating = rating
        self.rank = rank
        self.captain = captain

    def __init__(self, data: dict):
        self.id = data.get('id')
        self.name = data.get('name')
        self.rating = data.get('rating')
        self.rank = data.get('rank')
        self.captain = Player(data.get('captain', {}))

    def _to_message(self):
        msg = "```json"
        msg += "\nTEAM:\n"
        msg += "\tId: {}\n"
        msg += "\tName: {}\n"
        msg += "\tRank: {}\n"
        msg += "\tRating {}\n"
        msg += "\nCAPTAIN:\n"
        msg += "\tGuid: {}\n"
        msg += "\tName: {}\n"
        msg += "\tOnline: {}\n"
        msg += "```"
        return msg.format(self.id, self.name, self.rank, self.rating, self.captain.guid, self.captain.name, self.captain.online)
