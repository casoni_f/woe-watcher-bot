from xml.dom.expatbuilder import parseString
import requests
import asyncio
from discord.ext import commands
from datetime import datetime
from dateutil import parser

from models.woe.players import Player
from models.woe.teams import ArenaTeam


class WoE(commands.Cog):
    def __init__(self, bot, logger, config={}):
        self.bot = bot
        self.logger = logger
        self.config = config
        self.base_url = "https://realm.way-of-elendil.fr/api/realm"
        self.watch_list = []
        self.last_match = None
        self.alerter_channel = None
        self.launch_queue_alerter()

    @commands.command(name="top2s")
    async def get_top_2s(self, ctx: commands.Context, size=10):
        try:
            params = {
                'size': size
            }
            r = requests.get(
                self.base_url + "/arenas/teams/rankings/2", params=params)

            if r.status_code == 200:
                teams = r.json().get('content', [])
                if len(teams) == 0:
                    return await ctx.send("No teams to display.")
                msg = "```json\n"
                for i, team in enumerate(teams):
                    at = ArenaTeam(team)
                    msg += "{}. {} (id={}): {}\n".format(i + 1,
                                                         at.name, at.id, at.rating)
                msg += "```"
                await ctx.send(msg)
            else:
                self.logger.info(r.text)
                await ctx.send("Error loading top 2s.")
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="top3s")
    async def get_top_3s(self, ctx: commands.Context, size=10):
        try:
            params = {
                'size': size
            }
            r = requests.get(
                self.base_url + "/arenas/teams/rankings/3", params=params)

            if r.status_code == 200:
                teams = r.json().get('content', [])
                if len(teams) == 0:
                    return await ctx.send("No teams to display.")
                i = 0
                msg = "```json\n"
                for team in teams:
                    at = ArenaTeam(team)
                    i += 1
                    msg += "{}. {} (id={}): {}\n".format(i,
                                                         at.name, at.id, at.rating)
                msg += "```"
                await ctx.send(msg)
            else:
                await ctx.send("Error loading top 3s.")
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="team")
    async def get_arena_team_by_id(self, ctx: commands.Context, team_id: int):
        try:
            r = requests.get(self.base_url + "/arenas/teams/{}".format(team_id))

            if r.status_code == 200:
                data = r.json()
                await ctx.send(ArenaTeam(data)._to_message())
            else:
                await ctx.send("Team {} not found.".format(id))
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="search_team")
    async def search_arena_team(self, ctx: commands.Context, search_str: str):
        try:
            payload = {
                "name": search_str,
                "exact_name": False
            }
            r = requests.post(
                self.base_url + "/arenas/teams/searches", json=payload)
            if r.status_code == 200:
                msg = "```json"
                msg += "\nTEAMS:\n"
                teams = r.json().get('content', [])
                if len(teams) == 0:
                    return await ctx.send("No team found.")
                for team in teams:
                    at = ArenaTeam(team)
                    msg += "\t- {}({}): Rank {} ({})\n".format(at.name,
                                                               at.id, at.rank, at.rating)
                msg += "```"
                await ctx.send(msg)
            else:
                await ctx.send(r.text)
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="player")
    async def get_player_by_guid(self, ctx: commands.Context, guid: int):
        try:
            r = requests.get(self.base_url + "/characters/{}".format(guid))

            if r.status_code == 200:
                data = r.json()
                await ctx.send(Player(data)._to_message())
            else:
                await ctx.send("Player {} not found.".format(guid))
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="watchlist")
    async def get_watched_teams(self, ctx: commands.Context):
        try:
            msg = "```json"
            if len(self.watch_list) == 0:
                msg += "\nWatch List is empty !"
            else:
                msg += "\nWATCHED TEAMS:\n"
                for team in self.watch_list:
                    msg += "- {} ({})\n".format(team.name, team.id)
            msg += "```"
            await ctx.send(msg)
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="watch")
    async def add_watched_team(self, ctx: commands.Context, team_id: int):
        try:
            if team_id in self.watch_list:
                return await ctx.send("Team {} is already watched.".format(team_id))

            r = requests.get(self.base_url + "/arenas/teams/{}".format(team_id))
            if r.status_code == 200:
                team = ArenaTeam(r.json())
                self.watch_list.append(team)
                await ctx.send("Team {} (id={}) added to watchlist.".format(team.name, team.id))
            else:
                self.logger.error(r.text)
                await ctx.send("Team {} not found.".format(team_id))
        except Exception as e:
            self.logger.error(e)

    @commands.command(name="unwatch")
    async def delete_watched_team(self, ctx: commands.Context, team_id: int):
        try:
            r = requests.get(self.base_url + "/arenas/teams/{}".format(team_id))

            team = ArenaTeam(r.json()) if r.status_code == 200 else None

            if team is None or team not in self.watch_list:
                return await ctx.send("Team {} (id={}) is not watched anymore.".format(team.name, team.id))
            self.watch_list.remove(team)
            await ctx.send("Team {} removed.".format(team.id))

        except Exception as e:
            self.logger.error(e)

    @commands.command(name="alerter_channel")
    async def set_alerter_channel(self, ctx: commands.Context, channel_id: int):
        try:
            self.alerter_channel = self.bot.get_channel(channel_id)
            await ctx.send("Channel {} is now used for queue alerts.".format(channel_id))
        except Exception as e:
            self.logger.error(e)
            await ctx.send("Can't use channel {} for alerts".format(channel_id))

    def launch_queue_alerter(self):
        try:
            self.bot.loop.create_task(self.queue_alerter())
        except Exception as e:
            self.logger.error(e)

    async def queue_alerter(self):
        done = False
        while not done:
            try:
                if self.alerter_channel is not None:
                    is_new_match = False
                    r = requests.get(self.base_url + "/arenas/matches?page=0&size=1")
                    if r.status_code == 200:
                        msg = "```json"
                        msg += "\nQUEUE ALERT:"
                        data = r.json().get('content', [])
                        if len(data) > 0:
                            actual_match = data.pop()
                            if self.last_match is not None:
                                actual_date = parser.parse(actual_match['end_time'])
                                last_date = parser.parse(self.last_match['end_time'])
                                is_new_match = actual_date > last_date

                            if self.last_match is None or is_new_match:
                                self.last_match = actual_match
                                msg += "\nEnd Time: {}".format(parser.parse(actual_match['end_time']))
                                for team_match in actual_match.get('team_matches', []):
                                    team = ArenaTeam(team_match['team'])
                                    msg += "\n- {} (id={}): {}".format(team.name, team.id, team.rating)
                                msg += "```"
                                await self.alerter_channel.send(msg)
            except Exception as e:
                self.logger.error(e)
            finally:
                await asyncio.sleep(self.config.get('arena_refresh_rate', 10))