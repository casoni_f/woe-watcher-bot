#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import os
import time

import requests
from discord import Message
from discord.ext.commands import Bot, Context, errors
from dotenv import load_dotenv

from cogs.woe import WoE

description = '''WoE Watcher : A customized Discord bot by Pyroxy'''

bot = Bot(command_prefix="!", description=description)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

logger = logging.getLogger()

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


@bot.event
async def on_ready():
    logger.info('Logged in as ' + str(bot.user.name))


@bot.command()
async def chuck_facts(ctx: Context):
    """A Chuck Norris fact a day keeps the doctor away."""
    url = "http://api.icndb.com/jokes/random?"

    tmp = await ctx.send("Getting a Chuck Norris fact...")

    r = requests.get(url)
    if r.status_code == 200:
        fact = r.json()['value']
        content = "Fact " + str(fact['id']) + ": " + \
            fact['joke'].encode('latin').decode("latin")
        await tmp.edit(content=content)


@bot.event
async def on_command_error(ctx: Context, error):
    if isinstance(error, errors.CheckFailure):
        await ctx.send('You do not have the correct role for this command.')


# Here you can add more specific "on_message" actions
@bot.event
async def on_message(message: Message):
    await bot.process_commands(message)


try:
    load_dotenv()
    config = {
        'logging_level': os.getenv('LOGGING_LEVEL', logging.INFO),
        'arena_refresh_rate': int(os.getenv('ARENA_REFRESH_RATE', '10'))
    }
    logger.setLevel(config.get('logging_level', logging.INFO))
    bot.add_cog(WoE(bot, logger, config))
    bot.run(os.getenv('DISCORD_TOKEN'))
except Exception as e:
    print(e)
    print(time.time())
