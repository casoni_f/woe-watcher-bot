#!/usr/bin/env bash

docker build -t woe-watcher -f Dockerfile .
docker rm -f woe-watcher
docker run \
-d \
--rm \
--name woe-watcher \
--net=host \
-w /opt/woe-watcher \
woe-watcher
