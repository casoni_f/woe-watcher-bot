#!/usr/bin/env bash

docker build -t woe-watcher-dev -f Dockerfile.dev .
docker rm -f woe-watcher-dev
docker run \
-ti \
--rm \
--name woe-watcher-dev \
-v $(pwd):/opt/woe-watcher \
--net=host \
-w /opt/woe-watcher \
woe-watcher-dev
